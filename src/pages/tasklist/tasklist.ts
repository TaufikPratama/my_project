import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ItemSliding } from 'ionic-angular'; //mengatasi masalah tampilan sliding

/**
 * Generated class for the TasklistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tasklist',
  templateUrl: 'tasklist.html',
})
export class TasklistPage {

  tasks:Array<any>= [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.tasks = [
      {title:'Black Clover', status:'open'},
      {title:'Infite Dendrogram', status:'open'},
      {title:'Naruto Shipuden', status:'open'},
      {title:'One Piece', status:'open'},
      {title:'Sword Art Online', status:'open'}
    ]
  }

  addItem() {
    let theNewTask: string = prompt ("New Task");
    if (theNewTask!='') {
      this.tasks.push({title: theNewTask, status: 'open'})
    }

  }

  markAsDone(slidingItem: ItemSliding, task:any){
    task.status = 'done';
    slidingItem.close();
  }

  removeTask(slidingItem: ItemSliding, task){
    task.status='removed';
    let index = this.tasks.indexOf(task);
    if (index >-1){
    this.tasks.splice(index,1);
    }
    slidingItem.close();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TasklistPage');
  }

}
